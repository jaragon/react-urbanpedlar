<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-explore">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Explore the store!</h2>
      </div>

      <div class="row explore-feature-container">
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-12">
              <div class="explore-feature" style="background-image: url('img/explore-hero1.jpg'); min-height: 250px;">
                <div>
                  <h1 style="color: #a475b7;">Gifts <br> for Mom</h1>
                  <br>
                  <a href="explore-featured-product.php" class="button button-violet">shop now</a>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-5">
              <div class="explore-feature" style="background-image: url('img/explore-feature1.jpg')">
                <div>
                  <br>
                  <h3>Plant Pots, <br> Green</h3>
                  <br>
                  <a href="shop.php" class="button button-green">shop now</a>
                </div>
              </div>
            </div>

            <div class="col-md-7">
              <div class="explore-feature" style="background-image: url('img/explore-feature2.jpg'); color: #55565B;">
                <div>
                  <p style="font-size: 0.9em; margin-bottom: 0.5em;">FEATURED SELLER</p>
                  <h3 style="color: #84CFCD; margin-bottom: 0.5em;">Jane Doe</h3>
                  <p>Nullam quis risus eget urna mollis ornare vel eu leo.</p>
                  <br>
                  <a href="explore-featured-sellers.php" class="button button-seagreen">view shop</a></div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-4">
              <div class="explore-cat" style="background-image: url('img/explore-cat1.jpg')">
                <div><h3>USBs</h3></div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="explore-cat" style="background-image: url('img/explore-cat2.jpg')">
                <div><h3>Watches</h3></div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="explore-cat" style="background-image: url('img/explore-cat3.jpg')">
                <div><h3>Stuff</h3></div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-3">
          <?php include '_explore-sidebar.php'; ?>
        </div>
      </div>



    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
