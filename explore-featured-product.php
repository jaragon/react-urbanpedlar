<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-explore">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Our Featured Products</h2>
      </div>

      <div class="row">
        <div class="col-md-9">
          <div class="item-box-container">
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample2.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample3.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample4.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample2.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample3.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample4.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample2.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample3.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div class="item-box">
              <div class="item-box__img">
                <img src="img/search-sample4.jpg" alt="">
              </div>
              <div class="item-box__description">
                <h4 class="item-box__title">Product Name</h4>
                <p class="item-box__subtitle">AED 26.00</p>
              </div>
              <div class="item-box__action">
                <a href="#" class="button button-buy">buy</a>
                <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
              </div>
            </div>
            <div>
              <a href="#" class="button button-violet">view more</a>
            </div>
          </div>
        
        </div>

        <div class="col-md-3">
          <?php include '_explore-sidebar.php'; ?>
        </div>
      </div>


    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
