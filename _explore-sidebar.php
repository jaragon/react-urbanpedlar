<div class="explore-sidebar-container">
	<div class="search-bar search-bar--small">
		<form action="">
			<input type="text" placeholder="Search">
			<input type="submit">
		</form>
	</div>

	<div class="explore-sidebar">
		<h3>Our Featured</h3>
		<ul>
			<li><a href="explore-featured-product.php">Products</a></li>
			<li><a href="explore-featured-sellers.php">Sellers</a></li>
		</ul>

		<h3>Shop</h3>
		<ul>
			<li>
				<a href="">by category</a>
				<ul>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Category 1</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Category 2</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Category 3</span>
						</label>
					</li>
				</ul>
			</li>
			<li>
				<a href="">by product</a>
				<ul>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Product 1</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Product 2</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Product 3</span>
						</label>
					</li>
				</ul>
			</li>
			<li>
				<a href="">by seller</a>
				<ul>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Seller 1</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Seller 2</span>
						</label>
					</li>
					<li>
						<label class="checkbox-violet">
							<input type="checkbox">
							<span>Seller 3</span>
						</label>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>
