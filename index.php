<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-home">
  <div class="container">
    <div class="row">
      <div class="col-md-5 page-home-content">
        <h1>Welcome to <br> Urban Pedlar</h1>

        <p>Donec id elit non mi porta gravida at eget metus.
          Curabitur blandit tempus porttitor.</p>

        <p>Aenean lacinia bibendum nulla sed consectetur. Curabitur blandit tempus porttitor. Nulla vitae
          elit libero, a pharetra augue. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Curabitur blandit
          tempus porttitor.</p>
      </div>
      <div class="col-md-7 page-home-image">
        <img src="img/home-image1.png" alt="">
      </div>
    </div>
  </div>
  <a class="bottom-link" href="search.php">look for stuff<i class="arrow-down-white"></i></a>
</div>

<?php include '_footer.php'; ?>
