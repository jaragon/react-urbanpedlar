<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-explore">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Our Featured Sellers</h2>
      </div>

      <div class="row">
        <div class="col-md-9">
          <div class="filter-bar">
            <span class="filter-bar__title">Filter by:</span>
            <select class="filter-bar__select">
              <option selected disabled>by category:</option>
              <option>Wood</option>
              <option>Craft</option>
            </select>
          </div>
          
          <div class="seller-feature-box">
            <div class="seller-feature-box__info">
	            
							<div class="seller-feature-box__avatar">
								<img src="img/seller-avatar.jpg" alt="">
							</div>

							<div class="utility-float-left">
								<h3 class="seller-feature-box__title">
									Jon's Snowshop
								</h3>

								<div class="rating">
									<span class="active"></span>
									<span class="active"></span>
									<span class="active"></span>
									<span class="active"></span>
									<span></span>
								</div>
							</div>
              
              <p class="seller-feature-box__description">Coats, swords, shields. I know nothing.</p>

              <a href="#" class="button button-green">explore store</a>
            </div>
          
	          <div class="seller-feature-box__products">
		          <div class="seller-feature-box__product">
			          <img src="img/seller-feature-sample1.jpg" alt="">
		          </div>
		          <div class="seller-feature-box__product">
			          <img src="img/seller-feature-sample2.jpg" alt="">
		          </div>
		          <div class="seller-feature-box__product">
			          <img src="img/seller-feature-sample3.jpg" alt="">
		          </div>
		          
	          </div>
          </div>

	        <div class="seller-feature-box">
		        <div class="seller-feature-box__info">

			        <div class="seller-feature-box__avatar">
				        <img src="img/seller-avatar.jpg" alt="">
			        </div>

			        <div class="utility-float-left">
				        <h3 class="seller-feature-box__title">
					        Jon's Snowshop
				        </h3>

				        <div class="rating">
					        <span class="active"></span>
					        <span class="active"></span>
					        <span class="active"></span>
					        <span class="active"></span>
					        <span></span>
				        </div>
			        </div>

			        <p class="seller-feature-box__description">Coats, swords, shields. I know nothing.</p>

			        <a href="#" class="button button-green">explore store</a>
		        </div>

		        <div class="seller-feature-box__products">
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample1.jpg" alt="">
			        </div>
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample2.jpg" alt="">
			        </div>
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample3.jpg" alt="">
			        </div>

		        </div>
	        </div>

	        <div class="seller-feature-box">
		        <div class="seller-feature-box__info">

			        <div class="seller-feature-box__avatar">
				        <img src="img/seller-avatar.jpg" alt="">
			        </div>

			        <div class="utility-float-left">
				        <h3 class="seller-feature-box__title">
					        Jon's Snowshop
				        </h3>

				        <div class="rating">
					        <span class="active"></span>
					        <span class="active"></span>
					        <span class="active"></span>
					        <span class="active"></span>
					        <span></span>
				        </div>
			        </div>

			        <p class="seller-feature-box__description">Coats, swords, shields. I know nothing.</p>

			        <a href="#" class="button button-green">explore store</a>
		        </div>

		        <div class="seller-feature-box__products">
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample1.jpg" alt="">
			        </div>
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample2.jpg" alt="">
			        </div>
			        <div class="seller-feature-box__product">
				        <img src="img/seller-feature-sample3.jpg" alt="">
			        </div>

		        </div>
	        </div>

        </div>

        <div class="col-md-3">
	        <?php include '_explore-sidebar.php'; ?>
				</div>

      </div>



    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
