<div class="helper">
	<p>Pages</p>
	<ul>
		<li><a href="index.php">Home</a></li>
		<li><a href="explore.php">Explore</a></li>
		<li><a href="explore-featured-product.php">Explore - Featured Product</a></li>
		<li><a href="explore-featured-sellers.php">Explore - Featured Sellers</a></li>
		<li><a href="generic.php">Generic</a></li>
		<li><a href="my-account.php">My Account</a></li>
		<li><a href="product-item.php">Product Item</a></li>
		<li><a href="search.php">Search</a></li>
		<li><a href="shop.php">Shop</a></li>
		<li><a href="seller.php">Seller</a></li>
		<li><a href="wishlist.php">Wishlist</a></li>
		<li><a href="wishlist-inside.php">Wishlist Inside</a></li>
		<li><a href="cart.php">Cart</a></li>
	</ul>
</div>
