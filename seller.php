<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-seller">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        
        <div class="seller-header">
          <div class="seller-header__cover"></div>

          <div class="seller-header__avatar">
            <img src="img/seller-avatar.jpg" alt="">
          </div>

          <div class="utility-float-left">
            <h3 class="seller-header__title">
              Jon's Snowshop
            </h3>
            <div class="rating">
              <span class="active"></span>
              <span class="active"></span>
              <span class="active"></span>
              <span class="active"></span>
              <span></span>
            </div>
          </div>
          
          <div class="utility-float-right">
            <div class="button button-yellow">Favorite Store</div>
            <div class="button button-seagreen2">Message</div>
          </div>
        </div>
        
        <div class="seller-header-bar">
          <div class="seller-header-bar__item">
            <a href="#">Products <span>23</span></a>
          </div>
          <div class="seller-header-bar__item">
            <a href="#">Reviews <span>2</span></a>
          </div>
          <div class="seller-header-bar__item">
            <a href="#">About</a>
          </div>
        </div>
        
        <div class="product-item-container">
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="product-item__description">
              <h4 class="product-item__name">Product Name</h4>
              <p class="product-item__price">AED 26.00</p>
            </div>
            <div class="product-item__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
