<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-shop">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Shop</h2>
      </div>
      <div class="row">
        <div class="col-md-9">
          <div class="product-item-container">
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample2.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample3.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample2.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample3.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
            <div class="item-box product-item product-item--small">
              <div class="product-item__img">
                <img src="img/search-sample1.jpg" alt="">
              </div>
              <div class="product-item__description">
                <h4 class="product-item__name">Product Name</h4>
                <p class="product-item__price">AED 26.00</p>
              </div>
              <div class="product-item__action">
                <a href="#" class="button button-green button--add">+</a>
              </div>
            </div>
          </div>
        </div>
      

        <div class="col-md-3">
          <?php include '_explore-sidebar.php'; ?>
        </div>
      </div>



    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
