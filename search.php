<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-search">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Search or choose a category <i class="arrow-down-white"></i></h2>
      </div>

      <div class="col-md-12">
        <div class="search-bar">
          <form action="">
            <input type="text" placeholder="What are you looking for?">
            <input type="submit">
          </form>
        </div>
      </div>

      <div class="col-md-3 search-sidebar-container">

        <div class="search-sidebar">
          <ul>
            <li><a href="">Accessories</a></li>
            <li><a href="">Art & Collectibles</a></li>
            <li><a href="">Bags & Purses</a></li>
            <li><a href="">Bath & Beauty</a></li>
            <li><a href="">Books, Movies & Music</a></li>
            <li><a href="">Clothing</a></li>
            <li><a href="">Craft Supplies</a></li>
            <li><a href="">Electronics & Accessories</a></li>
            <li><a href="">Gifts</a></li>
            <li><a href="">Home & Living</a></li>
            <li><a href="">Jewellery</a></li>
            <li><a href="">Paper & Party Supplies</a></li>
            <li><a href="">Pet Supplies</a></li>
            <li><a href="">Shoes</a></li>
            <li><a href="">Toys & Games</a></li>
            <li><a href="">Weddings</a></li>
          </ul>
        </div>

        <div class="search-slidebar">
          <div class="search-slidebar-links">
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Ligula</a></li>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Ligula</a></li>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Ligula</a></li>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Ligula</a></li>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
            <div class="search-slidebar-item">
              <h3>Title</h3>
              <ul>
                <li><a href="#">Felis Euismod</a></li>
                <li><a href="#">Vivamus Sagittis</a></li>
                <li><a href="#">Lacus Vel</a></li>
                <li><a href="#">Laoreet Rutrum</a></li>
                <li><a href="#">Faucibus Dolor</a></li>
                <li><a href="#">Auctor</a></li>
              </ul>
            </div>
          </div>
          
          <div class="search-slidebar-featured">
            <h3>Featured Products</h3>
            <div>
              <div class="item-box item-box--horizontal item-box--fixedphoto">
                <div class="item-box__img">
                  <img src="img/search-sample1.jpg" alt="">
                </div>
                <div class="item-box__description">
                  <h4 class="item-box__title">Product Name</h4>
                  <p class="item-box__subtitle">AED 26.00</p>
                </div>
                <div class="item-box__action">
                  <a href="#" class="button button-buy">buy</a>
                  <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
                </div>
              </div>
              <div class="item-box item-box--horizontal item-box--fixedphoto">
                <div class="item-box__img">
                  <img src="img/search-sample2.jpg" alt="">
                </div>
                <div class="item-box__description">
                  <h4 class="item-box__title">Product Name</h4>
                  <p class="item-box__subtitle">AED 26.00</p>
                </div>
                <div class="item-box__action">
                  <a href="#" class="button button-buy">buy</a>
                  <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
                </div>
              </div>

              <a href="" class="button button-violet search-slidebar-featured__viewmore">view more</a>

            </div>
          </div>
        </div>

      </div>

      <div class="col-md-9 featured-products-container">

        <h3>Featured Products</h3>
        <div class="item-box-container">
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample4.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample4.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>
          <div class="item-box">
            <div class="item-box__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-buy">buy</a>
              <a href="#" class="button button-wishlist"><i class="icon icon-wishlist"></i></a>
            </div>
          </div>


          <div>
            <a href="#" class="button button-violet">view more</a>
          </div>
        </div>

      </div>
    </div>
  </div>
  <a class="bottom-link bottom-link-wood" href="explore.php">Explore the store!</a>
</div>

<?php include '_footer.php'; ?>
