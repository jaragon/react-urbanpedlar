<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-wishlist">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Wishlist</h2>
        
        <div class="row">
          <div class="col-md-3">
            <div class="wishlist-info">
              <h3 class="wishlist-info__title">I Wantzzz</h3>
              <p class="wishlist-info__subtitle">Public List - 8 items</p>
              <a href="" class="wishlist-info__edit">Edit this list</a>
              <div class="wishlist-share">
                <p>Share with friends!</p>
                <ul>
                  <li><a href="#" class="wishlist-share__fb">Facebook</a></li>
                  <li><a href="#" class="wishlist-share__tw">Twitter</a></li>
                  <li><a href="#" class="wishlist-share__ig">Instagram</a></li>
                  <li><a href="#" class="wishlist-share__pi">Pinterest</a></li>
                  <li><a href="#" class="wishlist-share__em">Mail</a></li>
                </ul>
              </div>
            </div>
            
          </div>
          
          <div class="col-md-9 wishlist-item-container">
            <div class="wishlist-item">
              <div class="wishlist-item__image">
                <img src="img/product-item-view-thumb1.png" alt="">
              </div>

              <div class="wishlist-item__info">
                <div class="wishlist-item__title">
                  Product Name
                </div>
                <div class="wishlist-item__footer">
                  <div class="wishlist-item__price">AED 20.00</div>
                  <div class="button button-green">add to cart</div>
                </div>

              </div>
            </div>
            


            <div class="wishlist-item">
              <div class="wishlist-item__image">
                <img src="img/product-item-view-thumb1.png" alt="">
              </div>

              <div class="wishlist-item__info">
                <div class="wishlist-item__title">
                  Product Name
                </div>
                <div class="wishlist-item__footer">
                  <div class="wishlist-item__price">AED 20.00</div>
                  <div class="button button-green">add to cart</div>
                </div>

              </div>
            </div>

            <div class="wishlist-item">
              <div class="wishlist-item__image">
                <img src="img/product-item-view-thumb1.png" alt="">
              </div>

              <div class="wishlist-item__info">
                <div class="wishlist-item__title">
                  Product Name
                </div>
                <div class="wishlist-item__footer">
                  <div class="wishlist-item__price">AED 20.00</div>
                  <div class="button button-green">add to cart</div>
                </div>

              </div>
            </div>

            <div class="wishlist-item">
              <div class="wishlist-item__image">
                <img src="img/product-item-view-thumb1.png" alt="">
              </div>

              <div class="wishlist-item__info">
                <div class="wishlist-item__title">
                  Product Name
                </div>
                <div class="wishlist-item__footer">
                  <div class="wishlist-item__price">AED 20.00</div>
                  <div class="button button-green">add to cart</div>
                </div>

              </div>
            </div>

            <div class="wishlist-item">
              <div class="wishlist-item__image">
                <img src="img/product-item-view-thumb1.png" alt="">
              </div>
              
              <div class="wishlist-item__info">
                <div class="wishlist-item__title">
                  Product Name
                </div>
                <div class="wishlist-item__footer">
                  <div class="wishlist-item__price">AED 20.00</div>
                  <div class="button button-green">add to cart</div>
                </div>

              </div>
            </div>
          
          </div>
          
        </div>
        
      </div>
      </div>

    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
