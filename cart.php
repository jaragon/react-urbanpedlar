<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-cart">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Your Cart <img src="img/cart-title.png" width="90" alt=""></h2>
        <div class="row">

          <div class="col-md-9">
            <div class="cart-summary">
              <h3>2 items in your cart</h3>
              <div class="row">
                <div class="col-md-3">
                  <img src="img/cart-item-1.jpg" alt="">
                </div>
                <div class="col-md-9">
                  <div class="row">
                    <div class="col-md-6">
                      <h4>Product Name</h4>
                    </div>
                    <div class="col-md-6 price">
                      <h4>AED 26.00</h4>
                    </div>

                  </div>
                  <p>Sold by <a href="#">Bane's Funstore</a></p>
                  <select name="" id="">
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
                  <select name="" id="">
                    <option value="1">options</option>
                    <option value="2">2</option>
                  </select>
                  <input type="text" placeholder="additional instructions for the seller...">
                </div>
              </div>

              <hr>

              <div class="row">
                <div class="col-md-3">
                  <img src="img/cart-item-1.jpg" alt="">
                </div>
                <div class="col-md-9">
                  <div class="row">
                    <div class="col-md-6">
                      <h4>Product Name</h4>
                    </div>
                    <div class="col-md-6 price">
                      <h4>AED 26.00</h4>
                    </div>

                  </div>
                  <p>Sold by <a href="#">Bane's Funstore</a></p>
                  <select name="" id="">
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select>
                  <select name="" id="">
                    <option value="1">options</option>
                    <option value="2">2</option>
                  </select>
                  <input type="text" placeholder="additional instructions for the seller...">
                </div>
              </div>
            </div>

            <br>
            
            <h4 class="text-shadow">Products you might like</h4>
            <div class="product-item-container">

              <div class="item-box product-item product-item--small">
                <div class="product-item__img">
                  <img src="img/search-sample2.jpg" alt="">
                </div>
                <div class="item-box__description">
                  <h4 class="item-box__title">Product Name</h4>
                  <p class="item-box__subtitle">AED 26.00</p>
                </div>
                <div class="item-box__action">
                  <a href="#" class="button button-green button--add">+</a>
                </div>
              </div>

              <div class="item-box product-item product-item--small">
                <div class="product-item__img">
                  <img src="img/search-sample3.jpg" alt="">
                </div>
                <div class="item-box__description">
                  <h4 class="item-box__title">Product Name</h4>
                  <p class="item-box__subtitle">AED 26.00</p>
                </div>
                <div class="item-box__action">
                  <a href="#" class="button button-green button--add">+</a>
                </div>
              </div>

              <div class="item-box product-item product-item--small">
                <div class="product-item__img">
                  <img src="img/search-sample4.jpg" alt="">
                </div>
                <div class="item-box__description">
                  <h4 class="item-box__title">Product Name</h4>
                  <p class="item-box__subtitle">AED 26.00</p>
                </div>
                <div class="item-box__action">
                  <a href="#" class="button button-green button--add">+</a>
                </div>
              </div>
              
            </div>

          </div>
          <div class="col-md-3">
            <div class="cart-price-breakdown">
              <table>
                <tr>
                  <td>Amount</td>
                  <td>AED 52.00</td>
                </tr>
                <tr>
                  <td>Gift wrap</td>
                  <td>AED 5.00</td>
                </tr>
                <tfoot>
                <tr>
                  <td>Sub Total</td>
                  <td>AED 57.00</td>

                </tr>
                </tfoot>
              </table>
            </div>
            <a class="cart-checkout-button button button-green">Proceed to Checkout</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
