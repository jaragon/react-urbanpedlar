<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-product">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      </div>

      <div class="col-md-6">
        <div class="item-box product-view">
          <div class="item-box__img">
            <img src="img/product-item-view-image1.jpg" alt="">
          </div>
          <div class="product-view__thumb">
            <img src="img/product-item-view-thumb1.jpg" alt="">
            <img src="img/product-item-view-thumb2.jpg" alt="">
          </div>
          <div class="product-view__description">
            <h3>Description</h3>
            <p>Retro Receiver for NES. Connect and play on your NES system wirelessly using PS4, PS3, Wii Remote, Wii U Pro and all 8bitdo controllers</p>
            <p class="read-more">More information</p>
            <a class="js-read-more-toggle">Read More</a>
          </div>
        </div>

        <h4 class="text-shadow">Products you might like</h4>
        <div class="product-item-container">
          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample1.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>

          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample2.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>

          <div class="item-box product-item product-item--small">
            <div class="product-item__img">
              <img src="img/search-sample3.jpg" alt="">
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">Product Name</h4>
              <p class="item-box__subtitle">AED 26.00</p>
            </div>
            <div class="item-box__action">
              <a href="#" class="button button-green button--add">+</a>
            </div>
          </div>
        </div>
        
      </div>


      <div class="col-md-4">
        <div class="product-view-options bg-paper round-edge tape2">
          <h2>Product Name</h2>
          <p>AED 26.00</p>
          <div class="row">
            <div class="col-md-4">
              <select name="" id="">
                <option value="">Options</option>
              </select>
            </div>
            <div class="col-md-4">
              <select name="" id="">
                <option value="">Options</option>
              </select>
            </div>
            <div class="col-md-3"><input type="text" placeholder="Qty"></div>
          </div>

          <div class="row product-view-buttons">
            <div class="col-md-4">
              <a href="#" class="button button-green">add to cart</a>
            </div>
            <div class="col-md-8" style="text-align: right;">
              <a href="#" class="button button-yellow"><i class="icon icon-wishlist"></i>wishlist</a>
              <a href="#" class="button button-seagreen2"><i class="icon icon-share"></i>share</a>
            </div>
          </div>

        </div>

        <div class="product-view-options product-view-store-items bg-paper round-edge tape2">
          <div class="row">
            <div class="col-md-9">
              <h3>More from this store</h3>
            </div>
            <div class="col-md-3">
              <a href="#" class="button button-green button-small">view</a>
            </div>
          </div>
          <div class="row" style="padding: 0 1em;">
            <div class="col-xs-4">
              <a href=""><img src="img/product-item-view-more1.jpg" alt=""></a>
            </div>
            <div class="col-xs-4">
              <a href=""><img src="img/product-item-view-more2.jpg" alt=""></a>
            </div>
            <div class="col-xs-4">
              <a href=""><img src="img/product-item-view-more3.jpg" alt=""></a>
            </div>
          </div>
        </div>

        <div class="product-view-options product-view-reviews bg-paper round-edge tape2">
          <div class="row">
            <div class="col-xs-6">
              <h3>Reviews</h3>
            </div>
            <div class="col-xs-6" style="text-align: right">
              <a href="#" class="button button-green button-small">view</a>
            </div>
          </div>
          <hr class="shadow">
          <div class="row">
            <div class="col-xs-12">

              <div class="product-view-reviews-item">
                <div class="col-xs-2 product-view-reviews-item__image">
                  <img src="img/sample-my-account-avatar.png" alt="">
                </div>
                <div class="col-xs-10">
                  <div class="row">
                    <div class="col-md-6 product-view-reviews-item__name">Wade Wilson</div>
                    <div class="col-md-6 product-view-reviews-item__date">May 4, 2016</div>
                    <div class="col-md-12 product-view-reviews-item__description">
                      <p>Better than expected. Soft, light and well made.
                        I want to go on a Loft 415 shopping spree!</p>
                    </div>
                    <div class="col-md-12">
                      <div class="rating">
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class=""></span>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <div class="product-view-reviews-item">
                <div class="col-xs-2 product-view-reviews-item__image">
                  <img src="img/sample-my-account-avatar.png" alt="">
                </div>
                <div class="col-xs-10">
                  <div class="row">
                    <div class="col-md-6 product-view-reviews-item__name">Wade Wilson</div>
                    <div class="col-md-6 product-view-reviews-item__date">May 4, 2016</div>
                    <div class="col-md-12 product-view-reviews-item__description">
                      <p>Better than expected. Soft, light and well made.
                        I want to go on a Loft 415 shopping spree!</p>
                    </div>
                    <div class="col-md-12">
                      <div class="rating">
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class="active"></span>
                        <span class=""></span>
                      </div>
                    </div>
                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>

      </div>

      <div class="col-md-2">
        <a href="" class="product-view-close">Close</a>
      </div>



    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
