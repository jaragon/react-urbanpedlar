<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-my-account">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>My Account</h2>
      </div>
      <div class="col-md-6">
        <div class="my-account-profile">
          <div class="row">
            <div class="col-xs-3">
              <img src="img/sample-my-account-avatar.png" alt="">
            </div>
            <div class="col-xs-9">
              <h3>Profile Picture</h3>
              <span class="my-account-profile-upload">Change Picture: <button class="button">upload file</button></span>
              <p>Must be a .jpg, .gif or .png file smaller than 5MB and at least 400px by 400px.</p>
            </div>
            <div class="col-xs-12">
              <hr>
            </div>
            <div class="col-xs-12">
              <table>
                <tr>
                  <td>Name</td>
                  <td>Wade Wilson</td>
                </tr>
                <tr>
                  <td>Username</td>
                  <td>captaindeadpool</td>
                </tr>
                <tr>
                  <td>Gender</td>
                  <td>Male</td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td>NA</td>
                </tr>
                <tr>
                  <td>Birthday</td>
                  <td>6/26/1986</td>
                </tr>
                <tr>
                  <td>About</td>
                  <td>Me? No, no, no. Let's talk about you</td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td>NA</td>
                </tr>
              </table>
            </div>
            <div class="col-xs-12 my-account-profile-action">
              <button class="my-account-profile-edit">Edit Account</button>
              <button class="my-account-profile-deactivate">Deactivate Account</button>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="my-account-password">
          <h3>Password</h3>
          <form action="">
            <input type="password" placeholder="current password">
            <input type="password" placeholder="new password">
            <input type="password" placeholder="confirm new password">
            <input type="submit" value="change password" class="button">
          </form>
        </div>
        <div class="my-account-email">
          <h3>Email</h3>
          <table>
            <tr>
              <td>Current Email</td>
              <td>deadpoolfolyfe@gmail.com</td>
            </tr>
          </table>
          <hr>
          <h3>Change Email</h3>
          <form action="">
            <input type="email" placeholder="new email">
            <input type="email" placeholder="confirm new email">
            <input type="email" placeholder="your password">
            <input type="submit" value="change email" class="button">
          </form>
        </div>

      </div>
    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
