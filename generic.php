<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-generic">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Generic Pages</h2>
      </div>

      <div class="row generic-content">
        <div class="col-md-6">
          <h3>Purus Justo Egestas</h3>
          <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui.</p>
        </div>
        <div class="col-md-6">
          <h3>Bibendum Dapibus</h3>
          <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
        </div>
        <div class="col-xs-12">
          <hr>
        </div>
        <div class="col-xs-12">
          <p>Etiam porta sem malesuada magna mollis euismod. Etiam porta sem malesuada magna mollis euismod. Cras justo
            odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Maecenas
            faucibus mollis interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula
            porta felis euismod semper.</p>
          <p>Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Donec sed
            odio dui. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor
            fringilla. Vestibulum id ligula porta felis euismod semper. Duis mollis, est non commodo luctus, nisi erat
            porttitor ligula, eget lacinia odio sem nec elit.</p>
          <p>Donec sed odio dui. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam quis risus eget
            urna mollis ornare vel eu leo. Nullam quis risus eget urna mollis ornare vel eu leo. Vivamus sagittis lacus
            vel augue laoreet rutrum faucibus dolor auctor. Maecenas faucibus mollis interdum.</p>
          <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec sed odio dui. Curabitur blandit tempus
            porttitor. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
        </div>
      </div>
      
      <div class="col-md-4">
        <h3>Purus Justo Egestas</h3>
        <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui.</p>
      </div>
      <div class="col-md-4">
        <h3>Bibendum Dapibus</h3>
        <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
      </div>
      <div class="col-md-4">
        <h3>Bibendum Dapibus</h3>
        <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
      </div>
      <div class="col-md-3">
        <h3>Purus Justo Egestas</h3>
        <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec sed odio dui.</p>
      </div>
      <div class="col-md-3">
        <h3>Bibendum Dapibus</h3>
        <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
      </div>
      <div class="col-md-3">
        <h3>Bibendum Dapibus</h3>
        <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
      </div>
      <div class="col-md-3">
        <h3>Bibendum Dapibus</h3>
        <p>Maecenas faucibus mollis interdum. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam porta sem malesuada magna mollis euismod. Nulla vitae elit libero, a pharetra augue. </p>
      </div>



    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
