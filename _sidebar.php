<div class="sidebar-container">
	<div class="sidebar sidebar-primary">
		<div class="sidebar-scrollable">
			<div class="sidebar-section sidebar-logo">
				<a href="index.php"><img src="img/urbanpedlar-logo.png" alt=""></a>
				<p>Uncover something <br> uniquely you</p>
			</div>
			<div class="sidebar-section sidebar-login-link">
				<a href="#">Register or Log-in</a>
			</div>
			<div class="sidebar-bottom">
				<div class="sidebar-section sidebar-cart">
					<a href="cart.php">
						<div class="sidebar-cart-icon">
							<span>2</span>
						</div>
						Your Cart
					</a>
				</div>
				<div class="sidebar-section sidebar-social">
					<ul>
						<li><a href="#" class="facebook">Facebook</a></li>
						<li><a href="#" class="vimeo">Vimeo</a></li>
						<li><a href="#" class="linkedin">LinkedIn</a></li>
						<li><a href="#" class="youtube">YouTube</a></li>
					</ul>
				</div>
				<div class="sidebar-section sidebar-start-selling-today">
					<p>Start selling today</p>
					<ul>
						<li><a href="#">Register your shop</a></li>
						<li><a href="#">Shop log-in</a></li>
					</ul>
				</div>
			</div>

		</div>
		<div class="sidebar-menu">
			<div>
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="search.php">Shop</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Media</a></li>
					<li><a href="#">FAQs</a></li>
				</ul>
			</div>
			<a href="#" class="sidebar-menu-toggle"></a>
			<div class="sidebar-menu-help">
				<p>Click here to <br> open the menu</p>
			</div>
		</div>

		<div class="sidebar-reveal">
			<a class="sidebar-reveal-toggle">Reveal more</a>
			<ul>
				<li><a href="#">Terms &amp; Conditions</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="">Delivery Policy</a></li>
				<li><a href="">Return &amp; Refund Policy</a></li>
				<li><a href="">Contact</a></li>
			</ul>
		</div>
	
		<div class="sidebar-close">×</div>
	
	</div>

	<div class="sidebar sidebar-login">
		<div class="sidebar-scrollable">
			<div class="sidebar-section">
				<h3>Login</h3>
				<a href="#" class="sign-in-with facebook">Sign in with Facebook</a>
				<a href="#" class="sign-in-with google">Sign in with Google</a>
			</div>
			<div class="sidebar-section">
				<div class="sidebar-or">OR</div>
				<form action="" class="sidebar-form">
					<input type="text" placeholder="email or username">
					<input type="password" placeholder="password">
					<div class="row sidebar-form-bottom">
						<div class="col-xs-5">
							<input type="submit" value="log-in" class="button">
						</div>
						<div class="col-xs-7">
							New user? <br>
							<a href="#" onclick="showLoginRegisterForm('sidebar-register-active');">Register here</a>
						</div>
					</div>
				</form>
			</div>
			<div class="sidebar-section sidebar-login-footer">
				<a href="#">forgot password?</a>
				<a href="#">forgot email or username?</a>
			</div>
		</div>

		<div class="sidebar-close">×</div>
	</div>

	<div class="sidebar sidebar-register">
		<div class="sidebar-scrollable">
			<div class="sidebar-section">
				<h3>Register</h3>
				<a href="#" class="sign-in-with facebook">Sign in with Facebook</a>
				<a href="#" class="sign-in-with google">Sign in with Google</a>
			</div>
			<div class="sidebar-section">
				<div class="sidebar-or">OR</div>
				<form action="" class="sidebar-form">
					<input type="text" placeholder="first name">
					<input type="text" placeholder="last name">
					<input type="text" placeholder="username">
					<input type="text" placeholder="email">
					<input type="password" placeholder="password">
					<input type="password" placeholder="re-type password">
					<div class="sidebar-form-terms">
						<input type="checkbox">
						<span>I agree to Urban Pedlar’s <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>. Urban Pedlar may send you communications; you may change your preferences in your account settings.</span>
					</div>
					<div class="row sidebar-form-bottom">
						<div class="col-xs-5">
							<input type="submit" value="register" class="button">
						</div>
						<div class="col-xs-7">
							Already a user? <br>
							<a href="#" onclick="showLoginRegisterForm('sidebar-login-active');">Log-in here</a>
						</div>
					</div>
				</form>
			</div>

		</div>

		<div class="sidebar-close">×</div>
	</div>
</div>
<div class="sidebar-bg-overlay"></div>
