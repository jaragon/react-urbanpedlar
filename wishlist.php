<?php include '_header.php'; include '_sidebar.php'; ?>

<div class="page page-wishlist">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Wishlist</h2>
        <div class="wishlist-group-container">
          <div class="item-box item-box--4x4">
            <div class="item-box__img">
              <div>
                <img src="img/wishlist-sample2.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample3.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample4.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample5.jpg" alt="">
              </div>
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">I Wantzzzz</h4>
              <p class="item-box__subtitle">11 items</p>
            </div>
            <div class="item-box__action">
              <a href="wishlist-inside.php" class="button button-red">view list</a>
            </div>
          </div>
          <div class="item-box item-box--4x4">
            <div class="item-box__img">
              <div>
                <img src="img/wishlist-sample2.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample3.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample4.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample5.jpg" alt="">
              </div>
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">I Wantzzzz</h4>
              <p class="item-box__subtitle">11 items</p>
            </div>
            <div class="item-box__action">
              <a href="wishlist-inside.php" class="button button-red">view list</a>
            </div>
          </div>
          <div class="item-box item-box--4x4">
            <div class="item-box__img">
              <div>
                <img src="img/wishlist-sample2.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample3.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample4.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample5.jpg" alt="">
              </div>
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">I Wantzzzz</h4>
              <p class="item-box__subtitle">11 items</p>
            </div>
            <div class="item-box__action">
              <a href="wishlist-inside.php" class="button button-red">view list</a>
            </div>
          </div>
          <div class="item-box item-box--4x4">
            <div class="item-box__img">
              <div>
                <img src="img/wishlist-sample2.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample3.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample4.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample5.jpg" alt="">
              </div>
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">I Wantzzzz</h4>
              <p class="item-box__subtitle">11 items</p>
            </div>
            <div class="item-box__action">
              <a href="wishlist-inside.php" class="button button-red">view list</a>
            </div>
          </div>
          <div class="item-box item-box--4x4">
            <div class="item-box__img">
              <div>
                <img src="img/wishlist-sample2.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample3.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample4.jpg" alt="">
              </div>
              <div>
                <img src="img/wishlist-sample5.jpg" alt="">
              </div>
            </div>
            <div class="item-box__description">
              <h4 class="item-box__title">I Wantzzzz</h4>
              <p class="item-box__subtitle">11 items</p>
            </div>
            <div class="item-box__action">
              <a href="wishlist-inside.php" class="button button-red">view list</a>
            </div>
          </div>
          <div class="wishlist-group-add">
            <a href="#">
              <img src="img/wishlist-add-button.png" alt="">
              Create <br> New List
            </a>
          </div>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>

<?php include '_footer.php'; ?>
