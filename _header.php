<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Urban Pedlar</title>
	<link rel="stylesheet" href="css/app.css">
	<script src="js/vendor/jquery-3.1.0.js"></script>
	<script src="js/vendor/jquery-scrollspy.js"></script>
	<script src="js/vendor/macy.js"></script>
	<script src="js/app.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>

<?php include '_helper.php'; ?>