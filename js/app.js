$(function() {
  $('.sidebar-primary').on('click', function(e) {
    if ($(window).width() < 768) {
      e.stopPropagation();
      $(this).addClass('active');
    }
  });
  
  $('.sidebar-close').on('click', function(e) {
    e.stopPropagation();
    $('.sidebar').removeClass('active');
    $('.sidebar-container').removeClass('sidebar-login-active');
    $('.sidebar-container').removeClass('sidebar-register-active');
    $('.sidebar-bg-overlay').removeClass('active');
  });
  
  $('.sidebar-menu-toggle').on('click', function(e) {
    e.stopPropagation();
    $('.sidebar-menu').toggleClass('active');
  });
  
  $('.sidebar-reveal-toggle').on('click', function(e) {
    e.stopPropagation();
    var sidebar = $('.sidebar-reveal').toggleClass('active');
    var toggle = $('.sidebar-reveal-toggle');
    if (sidebar.hasClass('active')) {
      toggle.text('close');
    } else {
      toggle.text('Reveal more');
    }
  });

  $('.sidebar-login-link a').on('click', function() {
    setTimeout(function() {
      $('.sidebar-container').toggleClass('sidebar-login-active');
      $('.sidebar-bg-overlay').toggleClass('active');
    }, 10)
  });

  $('.sidebar-primary').on('click', function() {
    if (
      $('.sidebar-container').hasClass('sidebar-login-active') ||
      $('.sidebar-container').hasClass('sidebar-register-active')
    ) {
      $('.sidebar-container').removeClass('sidebar-login-active');
      $('.sidebar-container').removeClass('sidebar-register-active');
      $('.sidebar-bg-overlay').removeClass('active');
    }
  });
  
  if ($('.page').hasClass('page-home')) {
    $('.sidebar-menu-help').addClass('active');
  }
  
  $('.product-item--small img, .item-box--fixedphoto img').each(function() {
    var parent = $(this).parent();
    var img = $(this).attr('src');
    parent.css('background-image', 'url("' + img +'")');
    $(this).hide();
  });
  
  if ($('.page-home').length) {
    var $sidebarMenuHelp = $('.sidebar-menu-help');
    $(window).on('scroll', function(e) {
      if ($(window).scrollTop() != 0) {
        $sidebarMenuHelp.removeClass('active');
      }
      else {
        if ($('.page-home').length) {
          $sidebarMenuHelp.addClass('active');
        }
      }
    });
  }
  
  $('.helper').on('click', function() {
    $(this).addClass('active');
  });
  
  initializeBottomLinkPreloader();
  
  if ($('.explore-sidebar').length) {
    $('.explore-sidebar a').on('click', function(e) {
      e.preventDefault();
      
      console.log($(this).next().prop('nodeName'))
      
      if ($(this).next().prop('nodeName') == 'UL') {
        $(this).next().toggleClass('active');
      }
      else {
        window.location.href = $(this).attr('href');
      }
      
      return false;
    });
  }
  
  initializeMacy();
  
  $('.js-read-more-toggle').on('click', function() {
    console.log('haha')
    $(this).prev().slideToggle();
  });
  
  $('.search-sidebar a').on('click', function(e) {
    if ($(window).width() > 992) {
      e.preventDefault();
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.search-sidebar-container').removeClass('active');
      } else {
        Macy.init({
          container: '.search-slidebar-links',
          trueOrder: true,
          waitForImages: false,
          margin: 24,
          columns: 4,
          breakAt: {
            940: 2,
            500: 1
          }
        });
        $('.search-sidebar a').removeClass('active')
        $(this).addClass('active')
        $('.search-sidebar-container').addClass('active');
      }
      return false;
      
    }
  });
  
  
  // Explore sidebar stick on scroll

  $(window).scrollspy({
    min: $('.explore-sidebar-container').offset().top - 15,
    max: $(document).height(),
    onEnter: function(element, position) {
      $(".explore-sidebar-container").addClass('fixed');
    },
    onLeave: function(element, position) {
      $(".explore-sidebar-container").removeClass('fixed');
    }
  });
  
  
  

});

function showLoginRegisterForm(className) {
  var sidebarContainer = $('.sidebar-container');
  sidebarContainer.removeClass('sidebar-login-active');
  sidebarContainer.removeClass('sidebar-register-active');
  sidebarContainer.addClass(className);
}

function initializeMacy() {
  // Masonry

  Macy.init({
    container: '.item-box-container',
    trueOrder: true,
    waitForImages: false,
    margin: 24,
    columns: 3,
    breakAt: {
      940: 2,
      500: 1
    }
  });
  
  
}


function initializeBottomLinkPreloader() {
  // if has bottom-link, preload

  if ($('.bottom-link').length) {
    // preload page
    var newPage;
    var newPageTitle;

    var currentPage = $('.page');

    var url = $('.bottom-link').attr('href');

    $.ajax({
      url: url,
      dataType: 'html',
      success: function(data) {
        var html = $('<div/>').append($(data));
        newPage = html.find('.page');
        newPageTitle = html.find('title').text();
      }
    });

    $('.bottom-link').on('click', function(e) {
      e.preventDefault();
      $('body').append(newPage.addClass('scroll-preloaded'));

      initializeMacy();
      
      currentPage.addClass('scroll-up')

      $('.sidebar-menu-help').removeClass('active');

      setTimeout(function() {
        newPage.removeClass('scroll-preloaded');
      }, 0);

      setTimeout(function() {
        currentPage.remove();
        initializeBottomLinkPreloader();
      }, 1050);
      
      window.history.pushState(null, newPageTitle, url);
      $('title').text(newPageTitle);

      return false;
    });

  }
  
}


// Google Web Font

if (window.location.hostname != "localhost") {
  WebFontConfig = {
    google: { families: [ 'Rubik:400,300,500,700,900,400italic:latin' ] }
  };
  (function() {
    var wf = document.createElement('script');
    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
  })();
}

